package com.pawelbanasik.main;

import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.pawelbanasik.config.DemoConfig;
import com.pawelbanasik.dao.AccountDao;
import com.pawelbanasik.domain.Account;

public class AfterFinallyDemoApp {

	public static void main(String[] args) {

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DemoConfig.class);
		AccountDao accountDao = context.getBean("accountDao", AccountDao.class);
		List<Account> accounts = null;
		try {
			boolean tripWire = true;
			accounts = accountDao.findAccounts(tripWire);
		} catch (Exception e) {
			System.out.println("\n\nMain Program ... caught exception: " + e);
		}
		System.out.println("\n\nMain Program: AfterThrowingDemoApp");
		System.out.println("----");
		System.out.println(accounts);
		context.close();

	}

}
